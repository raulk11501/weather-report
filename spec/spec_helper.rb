require 'webmock/rspec'
WebMock.disable_net_connect!(allow_localhost: true)
RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  # rspec-mocks config goes here. You can use an alternate test double
  # library (such as bogus or mocha) by changing the `mock_with` option here.
  config.mock_with :rspec do |mocks|
    # Prevents you from mocking or stubbing a method that does not exist on
    # a real object. This is generally recommended, and will default to
    # `true` in RSpec 4.
    mocks.verify_partial_doubles = true
  end

  config.around(:each, :caching) do |example|
    caching = ActionController::Base.perform_caching
    ActionController::Base.perform_caching = example.metadata[:caching]
    example.run
    Rails.cache.clear
    ActionController::Base.perform_caching = caching
  end

  config.before(:each) do
    stub_request(:get, WEATHER_STACK_URL).with(
        headers: {
          'Accept'=>'*/*', 'User-Agent'=>'Ruby'
        }
      ).to_return(
        status: 200, 
        body: {
          'current' => {
            'wind_speed'=> '70',
            "temperature_degrees"=> '40' 
          }          
        }.to_json, 
        headers: {}
      )
      stub_request(:get, OPEN_WEATHER_MAP_URL).with(
        headers: {
          'Accept'=>'*/*', 'User-Agent'=>'Ruby'
        }
      ).to_return(
        status: 200, 
        body: {
          'current' => {
            'wind_speed'=> '70',
            "temperature_degrees"=> '40' 
          }          
        }.to_json, 
        headers: {}
      )
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

end
