require 'rails_helper'
RSpec.describe "CacheData::Base", type: :module do
    
    let(:write_params) {{
        key: "test key",
        val: "test value"
    }}

    let(:read_params) {{
        key: "test key"
    }}

    context "write cache", :caching => true do 
        it 'returns value before 10 seconds' do 
            cache = CacheData::Base.new
            cache.write(write_params)
            # puts "no sleep to test cache"

            expect(
                cache.read(read_params)
            ).to eq("test value")
        end 

        it 'does not returns value after 10 seconds' do 
            cache = CacheData::Base.new
            cache.write(write_params)
            sleep 10
            # puts "sleep for 10 sec to test cache"
            expect(
                cache.read(read_params)
            ).to eq(nil)
        end 
    end
end
