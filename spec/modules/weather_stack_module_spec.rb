require 'rails_helper'
RSpec.describe "WeatherStack::Base", type: :request do
    context "fetch data from API" do 
        it 'returns data if status sucess' do 
            report = WeatherStack::Base.new.formatted_report
            expect(report.keys).to include(
                :wind_speed,
                :temperature_degrees
            )
        end 

        it 'does not returns data if status fails' do 
            stub_request(:get, WEATHER_STACK_URL).with(
                headers: {
                'Accept'=>'*/*', 'User-Agent'=>'Ruby'
                }
            ).to_return(
                status: 504, 
                body: "", 
                headers: {}
            )
            report = WeatherStack::Base.new.formatted_report
            expect(report).to be_empty
        end 
    end
end
