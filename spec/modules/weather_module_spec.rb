require 'rails_helper'
RSpec.describe "Weather", type: :request do
    describe "fetch weather report of city" do 
        it 'returns weather report with source' do 
            response = Weather.report
            expect(response.keys).to include(
                :source, 
                :data
            )
            expect(response[:data]).to include(
                :wind_speed,
                :temperature_degrees
            )
        end 
    end
end
