require 'rails_helper'
RSpec.describe "OpenWeatherMap::Base", type: :request do
    context "fetch data from API" do 
        it 'returns data if status is success' do 
            report = OpenWeatherMap::Base.new.formatted_report
            expect(report[:data].keys).to include(
                :wind_speed,
                :temperature_degrees
            )
        end 

        it 'returns data even status is fail' do 
            stub_request(:get, OPEN_WEATHER_MAP_URL).with(
                headers: {
                'Accept'=>'*/*', 'User-Agent'=>'Ruby'
                }
            ).to_return(
                status: 504, 
                body: "", 
                headers: {}
            )
            report = OpenWeatherMap::Base.new.formatted_report
            expect(report[:data].keys).to include(
                :wind_speed,
                :temperature_degrees
            )
        end 
    end
end
