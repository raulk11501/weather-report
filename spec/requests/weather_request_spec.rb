require 'rails_helper'

RSpec.describe "Weathers", type: :request do
    describe "fetch Merlbourne weather" do 
        it 'retruns status message' do 
            get('/v1/weather')
            json = JSON.parse(response.body)
            keys = json.keys
            expect(keys).to include(
                "wind_speed",
                "temperature_degrees"
            )
        end 
    end
end
