# README

* Please install the following

* Ruby version - 2.4.0 or higher

* Rails version - 5.0.3 or higher

* No Database configuration required

* Commands to run
    1. bundle install
    2. rails s
    3. curl "http://localhost:3000/v1/weather?city=melboune"

* How to run the test suite
    
* Test for Controllers
    `rspec spec/requests/weather_request_spec.rb`
* Test for Modules
    1. `rspec spec/modules/weather_module_spec.rb`
    2. `rspec spec/modules/cache_data_module_spec.rb`
    3. `rspec spec/modules/weather_stack_module_spec.rb`
    4. `rspec spec/modules/open_weather_map_module_spec.rb`
* Run follwing command to run the whole spec at once
    1. `rspec`


