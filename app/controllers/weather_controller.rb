class WeatherController < ApplicationController
    def index
        response = Weather.report
        render json: response[:data]
    end
end
