module CacheData
    class Base

        def initialize(api_key: nil)
            @cache_store = ActiveSupport::Cache::MemoryStore.new
        end

        def write(key: nil, val: nil)
            @cache_store.write(key, val, expires_in: 10.seconds)
        end
        
        def read(key: nil)
            @cache_store.read(key)
        end
        
    end
end
  