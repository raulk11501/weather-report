############
#TODO API key isn't working for OPEN_WEATHER_MAP_URL ??
####
module OpenWeatherMap
    class Base

        def initialize(api_key: nil)
            # @api_key = api_key
        end

        def formatted_report(city=nil)
            format_data HTTParty.get(OPEN_WEATHER_MAP_URL)
        end
        
        private

        def format_data(response)
            if response.present? && response.code==200
                response = JSON.parse(response) if response.parsed_response.is_a?(String)
                
                data = {
                    "wind_speed": response.as_json["current"]["wind_speed"],
                    "temperature_degrees": response.as_json["current"]["temperature"]
                }
                return {data: data, source: "open_weather_map"}
            else
                data = {
                    "wind_speed": 100, 
                    "temperature_degrees": 100
                }
                return {data: data, source: "open_weather_map"}
            end
        end
  
    end
end
  