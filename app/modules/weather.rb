module Weather
  class << self
    
    def report(city: nil)
      object = {key: "weather_report"}
      data = CacheData::Base.new.read(object)
      if data.present? 
        return {data: JSON.parse(data), source: "cache"}
      else
        return report_and_cache
      end
    end

    def report_and_cache
      report = get_api_report
      object = {
        key: "weather_report",
        val: report.to_json
      }
      CacheData::Base.new.write(object)
      return report
    end

    def get_api_report
      report = WeatherStack::Base.new.formatted_report
      if report.present?
        return {data: report, source: "weather_stack"} 
      else
        return OpenWeatherMap::Base.new.formatted_report
      end
    end
  end
end