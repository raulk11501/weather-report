module WeatherStack
    class Base

        def initialize(api_key: nil)
            # @api_key = api_key
        end

        def formatted_report(city=nil)
            format_data HTTParty.get(WEATHER_STACK_URL)
        end
        
        private

        def format_data(response)
            if response.present? && response.code==200
                response = JSON.parse(response) if response.parsed_response.is_a?(String)
                return {
                    "wind_speed": response.as_json["current"]["wind_speed"],
                    "temperature_degrees": response.as_json["current"]["temperature"]
                }
            else
                return {}
            end
        end
  
    end
end
  